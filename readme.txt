1. admin/categories.php

find: case 'move_category': and add above

case 'copy_category':	
	$categories = $db->Execute("select c.categories_id, cd.categories_name, cd.categories_description, c.categories_image,
									 c.parent_id, c.sort_order, c.date_added, c.last_modified,
									 c.categories_status
							  from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd
							  where c.categories_id = '" . (int)$_GET['cID'] . "'
							  and c.categories_id = cd.categories_id
							  and cd.language_id = '" . (int)$_SESSION['languages_id'] . "'");
	$cInfo = new objectInfo($categories->fields);

	$heading[] = array('text' => '<b>' . 'Copy Category' . '</b>');

	$contents = array('form' => zen_draw_form('categories', FILENAME_CATEGORIES, 'action=copy_category_confirm&cPath=' . $cPath) . zen_draw_hidden_field('categories_id', $cInfo->categories_id));
	$contents[] = array('text' => sprintf(TEXT_MOVE_CATEGORIES_INTRO, $cInfo->categories_name));
	$contents[] = array('text' => '<br />' . sprintf('Copy %s to:', $cInfo->categories_name) . '<br />' . zen_draw_pull_down_menu('copy_to_category_id', zen_get_category_tree(), $current_category_id));
	$contents[] = array('text' => '<br />' . TEXT_HOW_TO_COPY . '<br />' . zen_draw_radio_field('copy_as', 'link', true) . ' ' . 'Link' . '<br />' . zen_draw_radio_field('copy_as', 'duplicate') . ' ' . 'Copy');
	$contents[] = array('align' => 'center', 'text' => '<br />' . zen_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . zen_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id) . '">' . zen_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
	break;
	
--------------------------------------------------

find case 'set_categories_products_sort_order': and add above

case 'copy_category_confirm':
	$categories_id = $_POST['categories_id'];
	$copy_to_category_id = $_POST['copy_to_category_id'];
	
	// if this is categories link, add link and redirect
	if($_POST['copy_as'] == 'link') {
		$check = $db->Execute('SELECT COUNT(*) AS count FROM ' . DB_PREFIX . 'categories_to_categories WHERE linked_from_categories = ' . $categories_id . ' AND linked_to_categories = ' . $copy_to_category_id);
		if($check->fields['count'] > 0) {
			$messageStack->add_session('You had linked this category!', 'warning');
		} else {
			$sql_data_array = array('linked_to_categories' => $copy_to_category_id, 'linked_from_categories' => $categories_id);
			zen_db_perform(DB_PREFIX . 'categories_to_categories', $sql_data_array);
			$messageStack->add_session('You have just linked this category!', 'success');
		}
		zen_redirect(zen_href_link(FILENAME_CATEGORIES));
	}
	
	// processing when copy categories
	require(DIR_WS_CLASSES . 'simple_categories_tree.php');
	$sct = new SimpleCategoriesTree();
	$sct->init();
	
	// redirect if products valid
	$check = $db->Execute('SELECT COUNT(products_id) as count FROM ' . TABLE_PRODUCTS_TO_CATEGORIES . ' WHERE categories_id = ' . $copy_to_category_id);
	if($check->fields['count'] > 0) {
		$messageStack->add_session('You cannot move to this category because it has product, please select another category and try again!', 'caution');
		zen_redirect(zen_href_link(FILENAME_CATEGORIES));
	}
	
	// get last category id
	$last_category = $db->Execute('SELECT categories_id FROM ' . TABLE_CATEGORIES . ' ORDER BY categories_id DESC LIMIT 1');
	
	// get categories tree of $categories_id
	if($sct->category_tree[$categories_id]['sub'] == 'has_sub') {
		$_SESSION['category_array'] = array();
		$subs_categories_id = $sct->category_tree[$categories_id]['sub_cats'];
		foreach($subs_categories_id as $sub_category_id) {
			getCategoriesStringForParentCategoryId($sct->category_tree, $sub_category_id);
		}
	} else {
		// bof insert data into categories table
		
		// get meta categories
		$meta_categories = $db->Execute('SELECT * FROM ' . TABLE_METATAGS_CATEGORIES_DESCRIPTION . ' WHERE categories_id = ' . $categories_id);
		
		$categories_status = $db->Execute('SELECT categories_status FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $categories_id);
		$category_id = $categories_id + $last_category->fields['categories_id'];
		$sql_data_array = array(
												'categories_id' => $category_id,
												'parent_id' => $copy_to_category_id,
												'categories_status' => $categories_status->fields['categories_status'],
												'date_added' => 'now()'
											);
		zen_db_perform(TABLE_CATEGORIES, $sql_data_array);
		
		$sql_data_array = array(
												'categories_id' => $category_id,
												'categories_name' => $sct->category_tree[$categories_id]['name']
											);
		zen_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
		
		$sql_data_array = array(
												'categories_id' => $category_id,
												'metatags_title' => $meta_categories->fields['metatags_title'],
												'metatags_keywords' => $meta_categories->fields['metatags_keywords'],
												'metatags_description' => $meta_categories->fields['metatags_description']
											);
		zen_db_perform(TABLE_METATAGS_CATEGORIES_DESCRIPTION, $sql_data_array);
		// eof insert data into categories table
		
		$products = $db->Execute('SELECT DISTINCT products_id FROM ' . TABLE_PRODUCTS_TO_CATEGORIES . ' WHERE categories_id = ' . $categories_id);
		while(!$products->EOF) {
			$sql_data_array = array('products_id' => $products->fields['products_id'], 'categories_id' => $category_id);
			zen_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $sql_data_array);
			$products->MoveNext();
		}
		zen_redirect(zen_href_link(FILENAME_CATEGORIES));
	}
	
	// insert data into table if categories_id have subs
	$top_new_categories_id = $last_category->fields['categories_id'] + $categories_id;
	$top_new_categories_name = $sct->category_tree[$categories_id]['name'];
	
	// get meta categories
	$top_meta_categories = $db->Execute('SELECT * FROM ' . TABLE_METATAGS_CATEGORIES_DESCRIPTION . ' WHERE categories_id = ' . $categories_id);
	
	$top_new_categories_status = $db->Execute('SELECT categories_status FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $categories_id);
	
	// bof insert data into categories table with top categories have subs
	// categories table
	$sql_data_array = array(
											'categories_id' => $top_new_categories_id,
											'parent_id' => $copy_to_category_id,
											'categories_status' => $top_new_categories_status->fields['categories_status'],
											'date_added' => 'now()'
										);
	zen_db_perform(TABLE_CATEGORIES, $sql_data_array);
	// categories description
	$sql_data_array = array(
											'categories_id' => $top_new_categories_id,
											'categories_name' => $top_new_categories_name
										);
	zen_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
	// meta categories table
	$sql_data_array = array(
											'categories_id' => $top_new_categories_id,
											'metatags_title' => $top_meta_categories->fields['metatags_title'],
											'metatags_keywords' => $top_meta_categories->fields['metatags_keywords'],
											'metatags_description' => $top_meta_categories->fields['metatags_description']
										);
	zen_db_perform(TABLE_METATAGS_CATEGORIES_DESCRIPTION, $sql_data_array);
		
	// eof insert data into categories table
	
	foreach($_SESSION['category_array'] as $category_id => $category) {
		if(is_array($category)) {
		//echo $parent_id . '--' . $last_category->fields['categories_id'] . '---';
			$new_categories_id = $category_id + $last_category->fields['categories_id'];
			$new_categories_name = $sct->category_tree[$category_id]['name'];
			
			// get meta categories
			$new_meta_categories = $db->Execute('SELECT * FROM ' . TABLE_METATAGS_CATEGORIES_DESCRIPTION . ' WHERE categories_id = ' . $category_id);
			
			$new_categories_status = $db->Execute('SELECT categories_status FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $category_id);
			$check = $db->Execute('SELECT categories_id FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $new_categories_id);
			$new_parent_id = '';
			if($check->fields['categories_id'] > 0) {
				// use this parent id for sub categories
				$new_categories_id = $check->fields['categories_id'];
			} else {
				// bof insert data into categories table
				// insert data if categories id is invalid in database
				// categories table
				$sql_data_array = array(
														'categories_id' => $new_categories_id,
														'parent_id' => $top_new_categories_id,
														'categories_status' => $new_categories_status->fields['categories_status'],
														'date_added' => 'now()'
													);
				zen_db_perform(TABLE_CATEGORIES, $sql_data_array);
				// categories description table
				$sql_data_array = array(
														'categories_id' => $new_categories_id,
														'categories_name' => $new_categories_name
													);
				zen_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				// meta categories table
				$sql_data_array = array(
														'categories_id' => $new_categories_id,
														'metatags_title' => $new_meta_categories->fields['metatags_title'],
														'metatags_keywords' => $new_meta_categories->fields['metatags_keywords'],
														'metatags_description' => $new_meta_categories->fields['metatags_description']
													);
				zen_db_perform(TABLE_METATAGS_CATEGORIES_DESCRIPTION, $sql_data_array);
				// eof insert data into categories table
			}
			
			// insert here
			foreach($category as $sub_id => $sub_name) {
				$new_sub_id = $sub_id + $last_category->fields['categories_id'];
				
				// get meta categories
				$new_sub_meta_categories = $db->Execute('SELECT * FROM ' . TABLE_METATAGS_CATEGORIES_DESCRIPTION . ' WHERE categories_id = ' . $sub_id);
				
				$new_sub_status = $db->Execute('SELECT categories_status FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $sub_id);
				// bof insert data into categories table
				$sql_data_array = array(
														'categories_id' => $new_sub_id,
														'parent_id' => $new_categories_id,
														'categories_status' => $new_sub_status->fields['categories_status'],
														'date_added' => 'now()'
													);
				zen_db_perform(TABLE_CATEGORIES, $sql_data_array);
				$sql_data_array = array(
														'categories_id' => $new_sub_id,
														'categories_name' => $sub_name
													);
				zen_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				// meta categories table
				$sql_data_array = array(
														'categories_id' => $new_sub_id,
														'metatags_title' => $new_sub_meta_categories->fields['metatags_title'],
														'metatags_keywords' => $new_sub_meta_categories->fields['metatags_keywords'],
														'metatags_description' => $new_sub_meta_categories->fields['metatags_description']
													);
				zen_db_perform(TABLE_METATAGS_CATEGORIES_DESCRIPTION, $sql_data_array);
				// eof insert data into categories table
			}
		} else {
			$new_categories_id = $category_id + $last_category->fields['categories_id'];
			$new_categories_name = $sct->category_tree[$category_id]['name'];
			
			// get meta categories
			$new_meta_categories = $db->Execute('SELECT * FROM ' . TABLE_METATAGS_CATEGORIES_DESCRIPTION . ' WHERE categories_id = ' . $category_id);
			
			$new_categories_status = $db->Execute('SELECT categories_status FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $category_id);
			$check = $db->Execute('SELECT categories_id FROM ' . TABLE_CATEGORIES . ' WHERE categories_id = ' . $new_categories_id);
			if($check->fields['categories_id'] > 0) {
				$products = $db->Execute('SELECT DISTINCT products_id FROM ' . TABLE_PRODUCTS_TO_CATEGORIES . ' WHERE categories_id = ' . $category_id);
				while(!$products->EOF) {
					$sql_data_array = array('products_id' => $products->fields['products_id'], 'categories_id' => $new_categories_id);
					zen_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $sql_data_array);
					$products->MoveNext();
				}
			} else {
				// bof insert data into categories table
				// categories table
				$sql_data_array = array(
														'categories_id' => $new_categories_id,
														'parent_id' => $top_new_categories_id,
														'categories_status' => $new_categories_status->fields['categories_status'],
														'date_added' => 'now()'
													);
				zen_db_perform(TABLE_CATEGORIES, $sql_data_array);
				// categories description
				$sql_data_array = array(
														'categories_id' => $new_categories_id,
														'categories_name' => $new_categories_name
													);
				zen_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				// meta categories table
				$sql_data_array = array(
														'categories_id' => $new_categories_id,
														'metatags_title' => $new_meta_categories->fields['metatags_title'],
														'metatags_keywords' => $new_meta_categories->fields['metatags_keywords'],
														'metatags_description' => $new_meta_categories->fields['metatags_description']
													);
				zen_db_perform(TABLE_METATAGS_CATEGORIES_DESCRIPTION, $sql_data_array);
				// eof insert data into categories table
				$products = $db->Execute('SELECT DISTINCT products_id FROM ' . TABLE_PRODUCTS_TO_CATEGORIES . ' WHERE categories_id = ' . $category_id);
				while(!$products->EOF) {
					$sql_data_array = array('products_id' => $products->fields['products_id'], 'categories_id' => $new_categories_id);
					zen_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $sql_data_array);
					$products->MoveNext();
				}
			}
			//echo $category_id . '-' . $category;
			//echo '<br />';
		}
	}
	$messageStack->add_session('You have duplicated this category!', 'success');
	zen_redirect(zen_href_link(FILENAME_CATEGORIES));
break;

--------------------
2. admin/includes/modules/category_product_listing.php

find <?php echo '<a href="' . zen_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&cID=' . $categories->fields['categories_id'] . '&action=move_category') . '">' . zen_image(DIR_WS_IMAGES . 'icon_move.gif', ICON_MOVE) . '</a>'; ?>

add below
<?php echo '<a href="' . zen_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&cID=' . $categories->fields['categories_id'] . '&action=copy_category') . '">' . zen_image(DIR_WS_IMAGES . 'icon_copy_to.gif', 'Copy') . '</a>'; ?>
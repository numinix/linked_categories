<?php
/**
 * new arrivals-products functions
 *
 * @package functions
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: new_arrivals.php 2011-06-12  numnix $
 */

function getCategoriesStringForParentCategoryId($tree_array, $category_id) {
	if($tree_array[$category_id]['sub'] == 'has_sub') {
		$subs = $tree_array[$category_id]['sub_cats'];
		foreach($subs as $sub_id) {
			$_SESSION['category_array'][$category_id][$sub_id ] = $tree_array[$sub_id]['name'];
			getCategoriesStringForParentCategoryId($tree_array, $sub_id);
		}
	} else {
		$_SESSION['category_array'][$category_id] = $tree_array[$category_id]['name'];
	}
}
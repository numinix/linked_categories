<?php
$content = '<div id="categoriesListSidebox" class="sideBoxContent">';
$categoryDefaultId = 0; // coupon
if( isset($_GET['cPath']) && !empty($_GET['cPath']) ) {
  $categoryTree = $_SESSION['category_tree']->retrieveCategoriesTreeArray();
  $categoryDefaultId = end(explode('_', $_GET['cPath']));
  //echo '<pre>';var_dump($categoryTree[$categoryDefaultId]);
  if($categoryTree[$categoryDefaultId]['sub'] == 'no_sub') {
    $categoryDefaultId = $categoryTree[$categoryDefaultId]['parent_id'];
  }
}
//$content .= '<h5><a href="">' . $categoryTree[$categoryDefaultId]['name'] . '</a></h5>';
$content .= $_SESSION['category_tree']->buildCategoryString('<ul class="{class}">{child}</ul>', '<li class="{class}"><a class="{class} category-top" href="{link}"><span class="{class}">{name}</span></a>{child}</li>', $categoryDefaultId, 0, 1);
$content .= '</div>';